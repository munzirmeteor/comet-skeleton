<?php
namespace MTCSB\Comet\Package\Http\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class CometPackageServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Providers to register.
     * @var array
     */
    protected $providers = [
        
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerRoutes();
        $this->registerViews();
    }


    /**
     * Register views
     */
    public function registerViews()
    {
        /*
		$this->loadViewsFrom(__DIR__ . '/../../../resources/views/', 'comet.package');
        $this->publishes([
            __DIR__ . '/../../../resources/views' => base_path('resources/views/vendor/comet.package'),
            __DIR__ . '/../../../resources/sass' => base_path('resources/assets/sass/vendor/comet.package'),
        ]);
		*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $aliases = [
            
        ];

        $this->registerProviders();
        $this->registerAliases($aliases);
	}

    /**
     * Register Aliases
     */
    public function registerAliases(array $alias)
    {
        AliasLoader::getInstance($alias);
    }

    /**
     * Register providers
     */
    public function registerProviders()
    {
        foreach ($this->providers as $providerClass) {
            $this->app->register($providerClass);
        }
    }

    /**
     * Register Routes
     */
    public function registerRoutes()
    {
        require __DIR__ . '/../routes.php';

        if ( file_exists(__DIR__ .'/../api_routes.php') )
        {
            require __DIR__ . '/../api_routes.php';
        }
    }
}
